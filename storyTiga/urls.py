from django.urls import path
from . import views

app_name = 'storyTiga'

urlpatterns = [
    path('', views.index, name='index'),
    path('cv/', views.cv, name='cv'),
]