from django.contrib import admin
from django.urls import path
from .views import schedule_index, schedule_create, remove_schedule, schedule_detail

urlpatterns = [
    path('schedule/', schedule_index, name='schedule' ),
    path('schedule-create/', schedule_create, name='scheduleCreate' ),
    path('schedule-detail/<int:id>', schedule_detail, name='scheduleDetail'),
    path('schedule-delete/<int:id>', remove_schedule, name="remove_schedule")
]

